/**
 * Created by aditya on 1/30/18.
 */

/**
 * Created by aditya on 1/29/18.
 */

import { createStore, applyMiddleware, compose } from 'redux';

import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';import allReducers from './reducers/index';

import { routerMiddleware } from 'react-router-redux';

import { composeWithDevTools } from 'redux-devtools-extension';

export const history = createHistory();

const middleware = composeWithDevTools(applyMiddleware(thunk, routerMiddleware(history)));

const store = createStore(
    allReducers,
    middleware
);

export default store;
