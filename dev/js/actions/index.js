/**
 * Created by aditya on 1/29/18.
 */

export const selectCard = (card) => {
    console.log("You clicked on card : ", card.title);
    return {
        type: "CARD_SELECTED",
        payload: card
    }
};

export const selectAddCard = () => {
    console.log("Wohooo you entered to create a Card baiby :P");
    return {
        type: "CARD_ADD_SELECTED",
        payload: {}
    }
};

// TODO: change it do that it can add cards to state.board instead of state.cards.
export const addCard = (card) => {
    console.log("You clicked on card : ", card);
    return {
        type: "CARD_CREATE",
        payload: card
    }
};

export const addList = (name) => {
    console.log("you are trying to add a list.");
    let list = {
        name: name,
        cards: []
    };
    return {
        type: "LIST_CREATE",
        payload: list
    }
};
