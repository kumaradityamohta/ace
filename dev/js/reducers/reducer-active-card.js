/**
 * Created by aditya on 1/29/18.
 */

export default function (state=null, action) {
    console.log("Active Cards reducer's State : ", state);

    switch (action.type) {
        case "CARD_SELECTED": return action.payload;
    }
    return state;
}
