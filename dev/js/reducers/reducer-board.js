/**
 * Created by aditya on 1/31/18.
 */


export default function(state=null, action){

    switch(action.type) {
        case "LIST_CREATE":
            if (!state) return [action.payload];
            return [
                ...state,
                action.payload
            ];
        case "CARD_CREATE":
            let card_payload = action.payload;
            let listId = card_payload.listId;
            let card = card_payload.card;
            // creating a new lists object with updated card.
            let lists = state.map(function(list) {
                if (list.id === listId) {
                    if (list.cards.length > 0) card.id = list.cards[list.cards.length - 1].id + 1;
                    else card.id = 1;
                    let new_cards = [...list.cards, card];
                    let new_list = {id:list.id, name:list.name, cards: new_cards};
                    console.log("new list is", new_list);
                    return new_list;
                }
                return list;
            });
            return lists;
    }

    if (!state) return [];
    return state;
}