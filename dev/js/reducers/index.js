/**
 * Created by aditya on 1/29/18.
 */

import { combineReducers } from 'redux';

import ActiveCardReducer from './reducer-active-card';
import BoardReducer from './reducer-board';


import { routerReducer } from 'react-router-redux';


const allReducers = combineReducers({
    board: BoardReducer,
    activeCard: ActiveCardReducer,
    router: routerReducer
});

export default allReducers;
