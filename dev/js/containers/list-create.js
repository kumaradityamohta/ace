/**
 * Created by aditya on 1/31/18.
 */


import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';

import { addList } from '../actions/index';


class ListCreate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: "",
            editing: false
        }
    }

    handleButtonFocus() {
        this.state.editing = true;
        this.setState(this.state);
    }
    handleButtonClick() {
        let name = this.state.name;
        this.props.addList(name);
    }
    handleInputChange(e) {
        this.state.name = e.target.value;
        this.setState(this.state);
    }
    handleInputBlur() {
        console.log("input on blurr");
        let board = this.props.board;

        let id = board.length > 0 ? board[board.length - 1].id + 1 : 1; // return max id+1 or return 1 if no list exists
        let name = this.state.name;
        this.state.name = "";
        this.state.editing = false;
        this.setState(this.state);

        if(name.length > 0) this.props.addList(id, name);
    }

    renderListCreateCard() {
        if (this.state.editing) {
            return (
                <div
                    className="list-create-card">
                    <input
                        type="text"
                        placeholder="List Name"
                        value={this.state.name}
                        onChange={(e) => this.handleInputChange(e)}
                        onBlur={()=>this.handleInputBlur()}
                        autoFocus={this.state.editing}/>
                    <button
                        disabled={this.state.name.length <= 0}
                        onClick={() => this.handleButtonClick()}>
                        <FontAwesome name="check" size="2x"/>
                    </button>
                </div>
            )
        }
        return (
            <button
                className="list-create-btn"
                onFocus={() => this.handleButtonFocus()}
            >Add List</button>
        )
    }
    render() {
        return (
            <div className="list-create">
                {this.renderListCreateCard()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        board: state.board
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators(
        {addList: addList},
        dispatch
    )
}

export default connect(mapStateToProps, matchDispatchToProps)(ListCreate);
