/**
 * Created by aditya on 1/31/18.
 */

import React from 'react';

import { connect } from 'react-redux';

import List from './list';
import ListCreate from './list-create';


class Board extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            height: 0
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        console.log("BOARD : ", props.board);
    }

    getWidth() {
        return  ((this.props.board.length + 1)*320).toString() + "px";
    }

    getLists() {
        return this.props.board.map((list, index) => <List key={list.id} list={list}/>);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({height: window.innerHeight-100});
    }

    render() {
        let style = {
            height: this.state.height,
            width: this.getWidth()
        };
        return (
            <div className="lists-container">
                <div className="lists" style={style}>
                    {this.getLists()}
                    <ListCreate/>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        board: state.board
    }
}


export default connect(mapStateToProps)(Board);