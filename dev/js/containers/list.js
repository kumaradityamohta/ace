/**
 * Created by aditya on 1/29/18.
 */

import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { Link } from 'react-router-dom';

import { addCard, selectCard } from '../actions/index';


class List extends React.Component {
    constructor(props) {
        super(props);
        console.log(props);

        this.state = {
            cardCreate: false,
            new_card: {
                title: "",
                description: ""
            }
        }
    }
    getListName() {
        return this.props.list.name;
    }
    getCards() {
        let cards = this.props.list.cards;

        return cards.map(
            (card) => {
                const path = "cards/" + card.id.toString();
                return (
                    <li key={card.id} onClick={ () => this.props.selectCard(card)}>
                        <Link to={path}>
                            {card.title}
                        </Link>
                    </li>
                )
            }
        );
    }

    handleAddCardTitleChange(e) {
        this.state.new_card.title = e.target.value;
        this.setState(this.state);
    }

    handleCardCreateButton() {
        this.state.cardCreate = true;
        this.setState(this.state);
    }
    handleCancelCardButton() {
        this.state.cardCreate = false;
        this.state.new_card = {title: "", description: ""};
        this.setState(this.state);
    }

    handleAddCardButton() {
        if (this.state.new_card.title.length > 0) {
            this.props.addCard(this.props.list.id, this.state.new_card);
            this.state.cardCreate = true;
            this.state.new_card = {title: "", description: ""};
            this.setState(this.state);
        }
    }

    showAddCard() {
        if (this.state.cardCreate)
            return (
                <li>
                    <input
                        autoFocus="true"
                        type="text"
                        value={this.state.new_card.title}
                        onChange={(e) => this.handleAddCardTitleChange(e)}/>
                </li>
            );
    }

    showAddCardButton() {
        if (this.state.cardCreate)
            return (
                <div>
                    <button
                        onClick={ () => this.handleAddCardButton() }
                        style={{width:'50%'}}
                        disabled={this.state.new_card.title.length <= 0}
                    > Add </button>
                    <button style={{width:'50%'}} onClick={ ()=> this.handleCancelCardButton()}>cancel</button>
                </div>
            );

        return (
            <button onClick={ () => this.handleCardCreateButton() }>
                Add Card
            </button>
        );
    }

    render() {
        return (
            <div className="card-list">
                <h4>{ this.getListName() }</h4>
                <ul>
                    { this.getCards() }
                    { this.showAddCard() }
                </ul>
                {this.showAddCardButton()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        // cards: state.cards,
    }
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            addCard: addCard,
            selectCard: selectCard
        }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(List);
