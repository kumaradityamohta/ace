/**
 * Created by aditya on 1/29/18.
 */

require('../../scss/main.scss');
require('../../scss/lists.scss');


import 'babel-polyfill';
import React from 'react';
import { Route, Link } from 'react-router-dom';

import FontAwesome from 'react-fontawesome';

import CardList from '../containers/card-list';
import CardDetail from '../containers/card-detail';
import CardCreate from '../containers/card-create';
import Board from '../containers/board-detail';

import Test from '../containers/test';


const App = () => (
    <div>
        <header>
            <Link to="/cards" className="logo">
                <FontAwesome name='home' size='2x'/>
            </Link>
        </header>
        <main>
            <Route exact path="/" component={Board}/>
            <Route exact path="/cards" component={CardList}/>
            <Route exact path="/cards/:id" component={CardDetail}/>
            <Route exact path="/card/create" component={CardCreate}/>
            {/*<Route exact path="/card/list" component={CardList}/>*/}
        </main>
    </div>
);

export default App;