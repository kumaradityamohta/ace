/**
 * Created by aditya on 1/31/18.
 */

import React from 'react';


export default class CardDescription extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <div>
                <textarea
                    rows="1"
                    cols="10"
                    placeholder="Card description"
                    onChange={(event) => this.props.handleChange(event)}
                    value={this.props.description}
                />
            </div>
        )
    }
}
