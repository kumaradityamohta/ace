/**
 * Created by aditya on 1/31/18.
 */

import React from 'react';


export default class CardTitle extends React.Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <div>
                <input
                    type="text"
                    placeholder="Card title"
                    value={this.props.title}
                    onChange={ (event) => this.props.handleChange(event) }/>
            </div>
        )
    }
}